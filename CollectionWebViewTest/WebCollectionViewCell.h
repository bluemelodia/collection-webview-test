//
//  WebCollectionViewCell.h
//  CollectionWebViewTest
//
//  Created by Melanie Lislie Hsu on 8/18/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebKit/WebKit.h"

@interface WebCollectionViewCell : UICollectionViewCell <WKNavigationDelegate>

- (void)loadWebsite:(NSString *)website;

@end
