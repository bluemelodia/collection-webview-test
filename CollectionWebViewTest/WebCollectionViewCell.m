//
//  WebCollectionViewCell.m
//  CollectionWebViewTest
//
//  Created by Melanie Lislie Hsu on 8/18/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import "WebCollectionViewCell.h"

@implementation WebCollectionViewCell {
    WKWebView *wkView;
    UIActivityIndicatorView *wkIndicator;
}

- (void)awakeFromNib {

}

- (void)dealloc {
    [wkView removeObserver:self forKeyPath:@"estimatedProgress"];
    NSLog(@"Web View dealloced");
}

- (void)loadWebsite:(NSString *)website {
    // Initialization code
    wkView = [[WKWebView alloc] init];
    wkView.navigationDelegate = self;
    wkView.scrollView.bounces = NO;
    [wkView.scrollView setBouncesZoom:NO];
    
    [self addSubView:wkView withConstraintWithParent:self];
    
    wkIndicator = [[UIActivityIndicatorView alloc] init];
    [wkIndicator.layer setBackgroundColor:[[UIColor colorWithWhite:0.0 alpha:1.0] CGColor]];
    wkIndicator.alpha = 0.10;
    
    [wkView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:NULL];
    [self addSubView:wkIndicator withConstraintWithParent:wkView];
    
    NSURL *URL = [[NSURL alloc] initWithString:website];
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:URL];
    [wkView loadRequest:nsrequest];
    [wkIndicator startAnimating];
}

/* stop the activity indicator once the page is fully loaded */
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"estimatedProgress"] && object == wkView) {
        // estimatedProgress is a value from 0.0 to 1.0
        NSLog(@"Progress: %f", wkView.estimatedProgress);
        if (wkView.estimatedProgress == 1.0) {
            [wkIndicator stopAnimating];
            [wkIndicator removeFromSuperview];
        }
    }
}

- (void)addSubView:(UIView*)subview withConstraintWithParent:(UIView*)parent {
    subview.translatesAutoresizingMaskIntoConstraints = NO;
    [parent addSubview:subview];
    [parent addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                       attribute:NSLayoutAttributeLeft
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:parent
                                                       attribute:NSLayoutAttributeLeft
                                                      multiplier:1.0
                                                        constant:0]];
    
    [parent addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                       attribute:NSLayoutAttributeRight
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:parent
                                                       attribute:NSLayoutAttributeRight
                                                      multiplier:1.0
                                                        constant:0]];
    [parent addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                       attribute:NSLayoutAttributeTop
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:parent
                                                       attribute:NSLayoutAttributeTop
                                                      multiplier:1.0
                                                        constant:0]];
    [parent addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                       attribute:NSLayoutAttributeBottom
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:parent
                                                       attribute:NSLayoutAttributeBottom
                                                      multiplier:1.0
                                                        constant:0]];
}

@end
