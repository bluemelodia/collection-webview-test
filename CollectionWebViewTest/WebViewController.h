//
//  WebViewController.h
//  CollectionWebViewTest
//
//  Created by Melanie Lislie Hsu on 8/18/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
