//
//  WebViewController.m
//  CollectionWebViewTest
//
//  Created by Melanie Lislie Hsu on 8/18/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import "WebViewController.h"
#import "WebCollectionViewCell.h"
#import "TheCollectionReusableView.h"

#define iDevice UI_USER_INTERFACE_IDIOM()
#define iPad     UIUserInterfaceIdiomPad
#define iPhone     UIUserInterfaceIdiomPhone

@interface WebViewController () {
    WKWebView *headerView;
    UIActivityIndicatorView *headerIndicator;
}

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self.collectionView registerClass:[WebCollectionViewCell class] forCellWithReuseIdentifier:@"WebCollectionViewCell"];

    UICollectionViewFlowLayout *layoutProduct = [[UICollectionViewFlowLayout alloc] init];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    layoutProduct.minimumLineSpacing = screenHeight*0.04;
    layoutProduct.headerReferenceSize = CGSizeMake(screenWidth, 44);
    
    [self.collectionView setContentInset:UIEdgeInsetsMake(15, 0, 0, 0)];
    
    /* customize the flow layout */
    if (iDevice == iPhone) {
        layoutProduct.sectionInset = UIEdgeInsetsMake(0, screenWidth*0.075, screenHeight*0.05, screenWidth*0.075);
        layoutProduct.itemSize = CGSizeMake(150, 165);
        layoutProduct.minimumInteritemSpacing = screenWidth*0.02;
    } else {
        layoutProduct.sectionInset = UIEdgeInsetsMake(0, screenWidth*0.06, screenHeight*0.05, screenWidth*0.06);
        layoutProduct.itemSize = CGSizeMake(200, 225);
        layoutProduct.minimumInteritemSpacing = screenWidth*0.04;
    }
    self.collectionView.collectionViewLayout = layoutProduct;
    
    UINib *headerNib = [UINib nibWithNibName:@"TheCollectionReusableView" bundle:nil];
    [self.collectionView registerNib:headerNib forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"TheCollectionReusableView"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    WebCollectionViewCell *cell = (WebCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"WebCollectionViewCell" forIndexPath:indexPath];
    
    switch (indexPath.row) {
        case 0:
            [cell loadWebsite:@"https://facebook.com/"];
            break;
        case 1:
            [cell loadWebsite:@"https://twitter.com/"];
            break;
        case 2:
            [cell loadWebsite:@"https://deviantart.com/"];
            break;
        case 3:
            [cell loadWebsite:@"https://github.com/"];
            break;
        case 4:
            [cell loadWebsite:@"https://google.com/"];
            break;
        case 5:
            [cell loadWebsite:@"https://yahoo.com/"];
            break;
        case 6:
            [cell loadWebsite:@"https://tumblr.com/"];
            break;
        case 7:
            [cell loadWebsite:@"https://microsoft.com/"];
            break;
        case 8:
            [cell loadWebsite:@"https://youtube.com/"];
            break;
        case 9:
            [cell loadWebsite:@"https://medium.com/"];
            break;
        default:
            break;
    }
    
    // Configure the cell
    cell.layer.borderColor = [[UIColor colorWithRed:229/255.0f green:233/255.0f blue:235/255.0f alpha:1.0]CGColor];
    cell.layer.borderWidth = 1.0f;
    cell.layer.cornerRadius = 2;
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    UICollectionReusableView *theView = [[UICollectionReusableView alloc] init];
    
    if (kind == UICollectionElementKindSectionHeader) {
            UICollectionReusableView *header = [self.collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"TheCollectionReusableView" forIndexPath:indexPath];
            theView = header;
            if (headerView == nil) {
                headerView = [[WKWebView alloc] init];
                
                [theView setFrame:CGRectMake(0.05*screenWidth+8, theView.frame.origin.y, screenWidth-2*(0.05*screenWidth+8), 80)];
                
                [headerView setFrame:CGRectMake(0, 0, theView.frame.size.width, theView.frame.size.height)];
                headerView.layer.borderColor = [[UIColor clearColor]CGColor];
                headerView.layer.borderWidth = 1.0f;
                headerView.layer.cornerRadius = 2;
                
                [self addSubView:headerView withConstraintWithParent:theView];
                
                headerIndicator = [[UIActivityIndicatorView alloc] init];
                [headerIndicator.layer setBackgroundColor:[[UIColor colorWithWhite:0.0 alpha:1.0] CGColor]];
                headerIndicator.alpha = 0.10;
                [self addSubView:headerIndicator withConstraintWithParent:headerView];

                NSURL *URL = [[NSURL alloc] initWithString:@""];
                NSURLRequest *nsrequest=[NSURLRequest requestWithURL:URL];
                [headerView loadRequest:nsrequest];
            }
    }
    return theView;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(0, 80);
}



- (void)addSubView:(UIView*)subview withConstraintWithParent:(UIView*)parent {
    subview.translatesAutoresizingMaskIntoConstraints = NO;
    [parent addSubview:subview];
    [parent addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                       attribute:NSLayoutAttributeLeft
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:parent
                                                       attribute:NSLayoutAttributeLeft
                                                      multiplier:1.0
                                                        constant:0]];
    
    [parent addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                       attribute:NSLayoutAttributeRight
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:parent
                                                       attribute:NSLayoutAttributeRight
                                                      multiplier:1.0
                                                        constant:0]];
    [parent addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                       attribute:NSLayoutAttributeTop
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:parent
                                                       attribute:NSLayoutAttributeTop
                                                      multiplier:1.0
                                                        constant:0]];
    [parent addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                       attribute:NSLayoutAttributeBottom
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:parent
                                                       attribute:NSLayoutAttributeBottom
                                                      multiplier:1.0
                                                        constant:0]];
}


#pragma mark <UICollectionViewDelegate>

/*
 // Uncomment this method to specify if the specified item should be highlighted during tracking
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
 }
 */

/*
 // Uncomment this method to specify if the specified item should be selected
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
 return YES;
 }
 */

/*
 // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
 }
 
 - (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
 }
 
 - (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
 }
 */

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
